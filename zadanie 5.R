library("C50")
library("MASS")
data(infert)
View(infert)
treeModel <- C5.0(x=infert[, -8], y=infert$education[1:8])
summary(treeModel)
plot(treeModel)

#zad2

install.packages("mlr")
install.packages("rFerns")
install.packages("randomForestSRC")
library("mlr")
library("rFerns")
library("randomForestSRC")


setwd("E:/Nowy folder (3)/R studio/zadanie 5/")

mac <- read.csv("macbook.csv")
mac$Ocena <- factor(mac$Ocena)

zadanie =
  makeClassifTask(id = deparse(substitute(mac)), mac, "Ocena",
                  weights = NULL, blocking = NULL, coordinates = NULL,
                  positive = NA_character_, fixup.data = "warn", check.data = TRUE)

ponowne_probkowanie = makeResampleDesc(method = "CV", iters = 10, stratify = FALSE)

metody_uczenia <- makeLearners(c("rpart", "C50","rFerns",
                                 "randomForestSRC"), type = "classif")

porownanie_metod_uczenia <- benchmark(learners = metody_uczenia,
                                      tasks = zadanie,
                                      resampling = ponowne_probkowanie)

porownanie_metod_uczenia

plotBMRBoxplots(porownanie_metod_uczenia, measure = mmce,
                order.lrn = getBMRLearnerIds(porownanie_metod_uczenia))

plotBMRSummary(porownanie_metod_uczenia)

plotBMRRanksAsBarChart(porownanie_metod_uczenia, pos = "dodge",
                       order.lrn = getBMRLearnerIds(porownanie_metod_uczenia))



